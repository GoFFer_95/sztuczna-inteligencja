﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using Medallion;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        Percepton siec;
        
        private int[] zestaw1 = new int[100] { 90, 38, 45, 75, 2, 68, 70, 67, 31, 69, 4, 9, 39, 44, 32, 30, 89, 66, 85, 20, 47, 84, 93, 83, 58, 62, 72, 96, 7, 97, 74, 17, 64, 13, 24, 11, 54, 55, 57, 41, 63, 1, 25, 6, 10, 78, 61, 60, 23, 33, 100, 18, 86, 42, 98, 37, 14, 21, 59, 43, 81, 53, 77, 94, 80, 46, 99, 26, 12, 8, 51, 76, 28, 34, 3, 92, 29, 82, 27, 87, 40, 16, 73, 65, 56, 91, 22, 50, 36, 5, 15, 35, 49, 19, 48, 95, 71, 52, 88, 79 };
        private int[] zestaw2 = new int[100] { 77, 53, 64, 50, 55, 17, 90, 65, 25, 93, 81, 97, 61, 11, 96, 69, 13, 92, 68, 14, 94, 35, 70, 34, 32, 36, 10, 85, 99, 74, 15, 98, 8, 7, 67, 76, 71, 80, 48, 82, 46, 41, 16, 51, 83, 49, 56, 2, 91, 19, 60, 84, 24, 100, 47, 30, 27, 42, 73, 43, 29, 62, 33, 78, 3, 1, 79, 9, 52, 39, 44, 58, 12, 88, 20, 66, 59, 31, 28, 4, 23, 6, 5, 75, 95, 72, 63, 89, 18, 40, 21, 37, 87, 38, 57, 86, 54, 26, 45, 22 };
        private int[] zestaw3 = new int[100] { 25, 52, 4, 44, 89, 12, 53, 64, 76, 54, 72, 18, 82, 42, 31, 69, 73, 39, 5, 80, 83, 50, 11, 74, 62, 17, 51, 92, 70, 14, 3, 45, 68, 55, 33, 19, 22, 91, 2, 46, 7, 26, 16, 90, 75, 65, 48, 88, 38, 36, 34, 78, 20, 13, 40, 67, 84, 85, 6, 77, 60, 98, 99, 47, 63, 97, 56, 28, 86, 95, 87, 27, 8, 15, 35, 96, 66, 49, 79, 30, 9, 93, 41, 71, 94, 58, 10, 24, 21, 43, 100, 37, 61, 23, 81, 1, 59, 29, 32, 57 };
        private List<double> zestawTreningowy;
        private List<double> zestawTreningowyWyjscia;
        public static Series series1;
        public static Series series2;
        private int[] zestaw = new int[100];

        public Form1()
        {
            InitializeComponent();
        }

        void trenuj(Percepton a, int ileEpok, int nauka, int trening, int numerZestaw)
        {
            int iloscNauka = nauka;
            int iloscTrening = trening;
            List<double> wejscia = new List<double>();
            List<double> wyjscia = new List<double>();
            zestawTreningowy = new List<double>();
            zestawTreningowyWyjscia = new List<double>();

            if (numerZestaw == 1)
            {
                zestaw = zestaw1;
            }
            if (numerZestaw == 2)
            {
                zestaw = zestaw2;
            }
            if (numerZestaw == 3)
            {
                zestaw = zestaw3;
            }

            for (int i = 0; i < iloscNauka; i++)
            {
                wejscia.Add(zestaw[i]);
                wyjscia.Add(Math.Sqrt(wejscia[i]));
            }
            for (int i = iloscNauka; i < iloscNauka+iloscTrening; i++)
            {
                zestawTreningowy.Add(zestaw[i]);
                zestawTreningowyWyjscia.Add(Math.Sqrt(zestaw[i]));
            }
            
            a.trenuj(wejscia, wyjscia, ileEpok, zestawTreningowy, zestawTreningowyWyjscia, nauka, trening);
        }


        private void button1_Click_1(object sender, EventArgs e)
        {
            
            siec = new Percepton(Convert.ToInt32(textBox1.Text), Convert.ToDouble(textBox4.Text));
            trenuj(siec, Convert.ToInt32(textBox2.Text), Convert.ToInt32(textBox3.Text), Convert.ToInt32(textBox5.Text), Convert.ToInt32(textBox6.Text));
        }

        private void button2_Click(object sender, EventArgs e)
        {
            chart1.Series.Clear();
           
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            chart1.Series.Clear();
            series1 = new System.Windows.Forms.DataVisualization.Charting.Series
            {
                Name = "Zestaw Uczący",
                Color = System.Drawing.Color.Green,
                IsVisibleInLegend = true,
                IsXValueIndexed = true,
                ChartType = SeriesChartType.Line
            };

            this.chart1.Series.Add(series1);

            series2 = new System.Windows.Forms.DataVisualization.Charting.Series
            {
                Name = "Zestaw Testowy",
                Color = System.Drawing.Color.Red,
                IsVisibleInLegend = true,
                IsXValueIndexed = true,
                ChartType = SeriesChartType.Line
            };

            this.chart1.Series.Add(series2);
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            chart1.Series.Clear();
        }
    }
}
