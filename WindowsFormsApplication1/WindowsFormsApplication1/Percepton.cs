﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Medallion;

namespace WindowsFormsApplication1
{
    class Percepton
    {
        int ileUkrytych;
        Neuron neuronWyjscia;
        double wejscie;
        double oczekiwaneWyjscie;
        double wyjscie;
        double wynik;
        double blad;
        double bladEpoki;
        bool flag = false;
        List<Neuron> warstwaUkryta = new List<Neuron>();
        private int iloscNauka;
        private int iloscTrening;

        public Percepton(int ileUkrytych, double wspNauki)
        {
            for (int i = 0; i < ileUkrytych; i++)
            {
                Neuron tmp = new Neuron();
                tmp.setWspNauki(wspNauki);
                warstwaUkryta.Add(tmp);
            }
             

            neuronWyjscia = new Neuron(ileUkrytych+1);
            neuronWyjscia.setWspNauki(wspNauki);
            this.ileUkrytych = ileUkrytych;
            bladEpoki = 0;
            
        }

        public double oblicz(double wejscie, double oczekiwaneWyjscie, bool czyUczyc)
        {
            this.oczekiwaneWyjscie = oczekiwaneWyjscie;
            this.wejscie = wejscie;

            //ustawienie wejść do neuronów ukrytych
            for (int i = 0; i < ileUkrytych; i++)
            {
                List<double> tmp = new List<double>();
                tmp.Add(1); //dodanie biasu do neuronu
                tmp.Add(wejscie);
                warstwaUkryta[i].ustawWejscia(tmp);
            }


            //obliczenie jakie wyniki zostaną wyplute przez neuron ukryty
            List<double> wynikUkrytej = new List<double>();
            foreach (Neuron x in warstwaUkryta)
                wynikUkrytej.Add(x.funkcjaAktywacji());


            //przekazanie na wejście neuronu WYJ wyjść z neuronów sieci ukrytej
            List<double> tmp2 = new List<double>();
            tmp2.Add(1);//dodanie biasu do neuronu wyjściowego
            foreach (double x in wynikUkrytej)
                tmp2.Add(x);
            neuronWyjscia.ustawWejscia(tmp2);


            //obliczenie jaki wynik nam zostanie wypluty przez sieć
            wyjscie = neuronWyjscia.suma(); //wynik sieci

            double bladSieci = oczekiwaneWyjscie - wyjscie; // różna z wyjścien z oczekiwanym wynikiem dla danego przypadku

            //jeżeli chcemy uczyć, to robimy poprawki
            if(czyUczyc)
            {
                double ObliczBladWartwyWyjsciowej = neuronWyjscia.ObliczBladWartwyWyjsciowej(bladSieci); // TO JEST TO POPRAWKI

                List<double> obliczBledyUkrytej = new List<double>();
                foreach( Neuron x in warstwaUkryta)
                {
                    obliczBledyUkrytej.Add(x.obliczBledyUkrytej(ObliczBladWartwyWyjsciowej));
                }


                //poprawki
                neuronWyjscia.POprawWagiWyjscie(ObliczBladWartwyWyjsciowej);

                for(int i =0; i < warstwaUkryta.Count; i++)
                {
                    warstwaUkryta[i].POprawWagiWyjscie(obliczBledyUkrytej[i]);
                }

               
            }

            return bladSieci;

            // a teraz liczymy jaki jest bład dla tego jednego przypadku
            //później uaktualniamy wagi

        }

        public void dodajBladWzorca(double bladWzorca)
        {
            this.bladEpoki += (Math.Pow(bladWzorca, 2));
        }

        

        public void trenuj(List<double> wejscia, List<double> wyjscia, int iloscEpok,
                        List<double> wejsciaTEST, List<double> wyjsciaTEST, int iloscNauka, int iloscTrening)
        {
            this.iloscNauka = iloscNauka;
            this.iloscTrening = iloscTrening;
            flag = false;
            for (int i = 0; i < iloscEpok; i++)
            {
                List<double> bledyTrening = new List<double>();
                List<double> bledyTest = new List<double>();
                double sumaBledyTrenig = 0;
                double sumaBledyTest = 0;
                for (int j = 0; j < wejscia.Count; j++)
                {
                    bledyTrening.Add(oblicz(wejscia[j], wyjscia[j], true)); // po każdym wzorcu liczymu koretkę
                   // bledyTest.Add(oblicz(wejsciaTEST[j], wyjsciaTEST[j], false)); // po każdym wzorcu liczymu koretkę
                }
                for (int j = 0; j < wejsciaTEST.Count; j++)
                {
                   // bledyTrening.Add(oblicz(wejscia[j], wyjscia[j], true)); // po każdym wzorcu liczymu koretkę
                    bledyTest.Add(oblicz(wejsciaTEST[j], wyjsciaTEST[j], false)); // po każdym wzorcu liczymu koretkę
                }

                foreach (double aa in bledyTrening)
                    sumaBledyTrenig += aa*aa;
                sumaBledyTrenig = sumaBledyTrenig/iloscNauka;
                foreach (double aa in bledyTest)
                    sumaBledyTest += aa*aa;
                sumaBledyTest = sumaBledyTest/iloscTrening;

                Form1.series1.Points.AddXY(i, sumaBledyTrenig/2);
                Form1.series2.Points.AddXY(i, sumaBledyTest/2);



                wejscia.Shuffle();
                wyjscia.Clear();
                for (int k = 0; k < wejscia.Count; k++)
                {
                    wyjscia.Add(Math.Sqrt(wejscia[k]));
                }
           //     Debug.WriteLine("wejscia" + wejscia[0] + " " + wejscia[1] + " " + wejscia[2]);
            //    Debug.WriteLine("wyjscia" + wyjscia[0] + " " + wyjscia[1] + " " + wyjscia[2]);
            }
            flag = true;
        }

        public double getWynik()
        {
            return wynik;
        }

        public double getBlad()
        {
            return blad;
        }
    }
}
