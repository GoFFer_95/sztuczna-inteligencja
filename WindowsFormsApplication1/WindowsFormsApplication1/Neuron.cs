﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    class Neuron
    {
        static Random rand = new Random();
        int ileWejsc;
        List<double> wejscia = new List<double>();
        List<double> wagi = new List<double>();
        double wspolczynnikUczenia = 0.001;

        public Neuron(int ileWejsc = 2)
        {
            //wejscia = List<double>(ileWejsc);
            //wagi = List<double>(ileWejsc);
            for (int i = 0; i < ileWejsc; i++)
            {
                wejscia.Add(new double());
                wagi.Add(new double());
            }
            this.ileWejsc = ileWejsc;
            this.losujWagi();
        }

        public void setWspNauki(double wspolczynnikUczenia)
        {
            this.wspolczynnikUczenia = wspolczynnikUczenia;
        }

        public double getDrugaWaga()
        {
            return wagi[1];
        }

        public double suma()
        {
            double wynik = 0.00;
            for (int i = 0; i < ileWejsc; i++)
                wynik += wejscia[i] * wagi[i];
            return wynik;
        }

        public double funkcjaAktywacji()
        {
            double s = suma();
            return (1.0 / (1 + Math.Exp(-s)));
        }

        public void ustawWejscia(List<double> wejscia)
        {
            if (wejscia.Count != this.wejscia.Count)
                Console.Out.WriteLine("Niezgodna dlugosc wektora wejsc");
            this.wejscia = wejscia;
        }

        public void ustawWagi(List<double> wagi)
        {
            if (wagi.Count != this.wagi.Count)
                Console.Out.WriteLine("Niezgodna dlugosc wektora wag");
            this.wagi = wagi;
        }

        public double pochodna()
        {
            return funkcjaAktywacji() * (1-funkcjaAktywacji());
        }

        public void losujWagi()
        {
           
            for (int i = 0; i < ileWejsc; i++)
            {
                wagi[i] = (((double)rand.Next(-500, 500)) / 1000.00) / 20.00;
            }
        }

        public double ObliczBladWartwyWyjsciowej(double bladWyjscia)
        {
            return bladWyjscia * pochodna();
        }

        public List<double> obliczBledyWYjscia(double bladWyjscioejNowy) //??
        {
            List<double> wynik = new List<double>();
            foreach (double x in wejscia)
                wynik.Add(x * bladWyjscioejNowy * pochodna());
            return wynik;
        }


        public double obliczBledyUkrytej(double obliczBledyWYjscia)
        {
            double suma = 0;

            for(int i = 0; i <ileWejsc; i++)
            {
                suma = suma + (obliczBledyWYjscia * wagi[i]);
            }

            return (suma * pochodna());
        }

        public void POprawWagiWyjscie( double blad)
        {
            for (int i = 0; i < ileWejsc; i++)
            {
                double korekta = wspolczynnikUczenia * blad * wejscia[i];
                wagi[i] += korekta;
            }
        }

        
    }
}
