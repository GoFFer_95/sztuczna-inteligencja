﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pietnastka
{
    class DFS
    {
        private const int MAX_RECURSION_DEPTH = 20;

        private Node startNode;
        char[] childsOrder;
        int depth;
        Solution sol;

        HashSet<Node> nodes;
        HashSet<Node> nodesVistited;

        public DFS(Node startNode, char[] childsOrder)
        {
            this.startNode = startNode;
            this.childsOrder = childsOrder;
            sol = new Solution();

            nodes = new HashSet<Node>();
            nodesVistited = new HashSet<Node>();
            depth = 0;
        }
        
        public Solution findSolution()
        {
            Node tempNode;

            nodes.Add(startNode);
            sol.ProcessedStates++;
            sol.startWorkingTime();

            while(nodes.Count !=0)
            { 
                startNode = nodes.Last();
                nodes.Remove(startNode);

                if (startNode.previousSteps.Length < MAX_RECURSION_DEPTH)
                {
                    foreach (char direction in childsOrder)
                    {
                        if (startNode.puzzle.CanIGo(direction))
                        {
                            tempNode = new Node(startNode.puzzle.newPuzzleAfterMove(direction), startNode.previousSteps + direction);

                            if (tempNode.puzzle.CheckStatus(tempNode.puzzle.Size, tempNode.puzzle.TabState))
                            {
                                //koniec programu
                                sol.stopWorkingTime();
                                startNode.previousSteps = startNode.previousSteps + direction;
                                sol.solution = startNode.previousSteps.Length + " " + startNode.previousSteps;
                                sol.solutionLength = startNode.previousSteps.Length;
                                if (startNode.previousSteps.Length > depth)
                                {
                                    sol.depth = startNode.previousSteps.Length;
                                }
                                else
                                {
                                    sol.depth = MAX_RECURSION_DEPTH;
                                }
                                return sol;
                            }
                            else if (!nodes.Contains(tempNode) && !nodesVistited.Contains(tempNode))
                            {
                                nodes.Add(tempNode);
                                if (tempNode.previousSteps.Length > depth)
                                {
                                    depth = startNode.previousSteps.Length;
                                }
                                sol.ProcessedStates++;

                            }
                        }

                    }
                   
                    nodesVistited.Add(startNode);
                    sol.VisitedStates++;
                }
            }
            sol.stopWorkingTime();

            sol.solution = "-1";
            sol.solutionLength = 0;
            sol.depth = depth;
                return sol;
        }
    }
}
