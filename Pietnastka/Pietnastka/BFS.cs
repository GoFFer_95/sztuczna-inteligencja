﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pietnastka
{
    class BFS
    {
        private Node startNode;
        char[] childsOrder;
        public Solution sol;
        Queue<Node> nodes;
        List<Node> nodesVistited;
        int depth;

        public BFS(Node startNode, char[] childsOrder)
        {
            this.startNode = startNode;
            this.childsOrder = childsOrder;
            sol = new Solution();
            nodes = new Queue<Node>();
            nodesVistited = new List<Node>();
            depth = 0;
        }


        public Solution findSolution()
        {
            Node tempNode;
            nodes.Enqueue(startNode); //dodanie elemntu do kolejki
            sol.ProcessedStates++;

            sol.startWorkingTime();
            while (nodes.Count != 0)
            {
                startNode = nodes.Dequeue();
               
                foreach (char direction in childsOrder)
                {

                    if (startNode.puzzle.CanIGo(direction))
                    {
                        tempNode = new Node(startNode.puzzle.newPuzzleAfterMove(direction), startNode.previousSteps + direction);
                      
                        if (tempNode.puzzle.CheckStatus(tempNode.puzzle.Size, tempNode.puzzle.TabState))
                        {
                            sol.stopWorkingTime();
                            startNode.previousSteps = startNode.previousSteps + direction;
                            sol.solution = startNode.previousSteps.Length + " " + startNode.previousSteps;
                            sol.solutionLength = startNode.previousSteps.Length;
                            if (startNode.previousSteps.Length > depth)
                            {
                                sol.depth = startNode.previousSteps.Length;
                            }
                            else
                            {
                                sol.depth = depth;
                            }
                            return sol;
                        }
                        else if (!nodes.Contains(tempNode) && !nodesVistited.Contains(tempNode))
                        {
                           
                            nodes.Enqueue(tempNode);
                            if (tempNode.previousSteps.Length > depth)
                            {
                                depth = startNode.previousSteps.Length;
                            }
                            sol.ProcessedStates++;
                      
                        }
                    }
                   
                }
                nodesVistited.Add(startNode);
                sol.VisitedStates++;
            }

            sol.stopWorkingTime();

            sol.solution = "-1";
            sol.solutionLength = 0;
            sol.depth = depth;
            return sol; 


        }

    }
}
