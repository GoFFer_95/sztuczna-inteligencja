﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pietnastka
{
    class Node
    {

        public Puzzle puzzle;

        public string previousSteps;  //lista wykonanych kroków
        public int depth; // głębokość jaka została zrobiona

        public Node(Puzzle puzzle)
        {
            this.puzzle = puzzle;
            previousSteps = null;
        }

        public Node(Puzzle puzzle, string solution)
        {
            this.puzzle = puzzle;
            previousSteps = solution;
        }


        public override bool Equals(Object other)
        {
            if (other == null)
            {
                return false;
            }

            Node nodeOther = other as Node;

            return puzzle.Equals(nodeOther.puzzle);

        }
        
        public override int GetHashCode()
        {
            return puzzle.GetHashCode();
        }



    }
}
