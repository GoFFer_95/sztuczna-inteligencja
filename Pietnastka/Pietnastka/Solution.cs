﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pietnastka
{
    class Solution
    {
        private int visitedStates;
        private int processedStates;
        // private List<Node> solution;

        public string solution;
        public int solutionLength;
        public int depth;


        private System.Diagnostics.Stopwatch workingTime;
        private System.Diagnostics.Stopwatch workingTime2;

        public Solution()
        {
            visitedStates = 0;
            processedStates = 0;
        //    solution = new List<Node>();
        }

        public int VisitedStates
        {
            get
            {
                return visitedStates;
            }
            set
            {
                visitedStates = value;
            }
        }

        public int ProcessedStates
        {
            get
            {
                return processedStates;
            }
            set
            {
                processedStates = value;
            }
        }


        public void startWorkingTime()
        {
            this.workingTime = System.Diagnostics.Stopwatch.StartNew();
        }

        public void stopWorkingTime()
        {
            workingTime.Stop();
        }
        public decimal getWorkingTime()
        {
            return decimal.Round((decimal)workingTime.Elapsed.TotalMilliseconds, 3);
        }

        public int lenghtSolution()
        {
            return solution.Length;
        }
    }
}