﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 * Przykłady jak odpalic program:
 * 
program bfs RDUL 4x4_05_00002.txt 4x4_01_0001_bfs_rdul_sol.txt 4x4_01_0001_bfs_rdul_stats.txt

program dfs LUDR 4x4_01_0001.txt 4x4_01_0001_dfs_ludr_sol.txt 4x4_01_0001_dfs_ludr_stats.txt

program astr manh 4x4_01_0001.txt 4x4_01_0001_astr_manh_sol.txt 4x4_01_0001_astr_manh_stats.txt
 */

namespace Pietnastka
{
    class Program
    {
        static void Main(string[] args)
        {

            char[] childsOrder = new char[4] { 'R', 'U', 'D', 'L' };
            char searchingAlgorithm = 'd';// d - dfs; b - bfs; a - astr
            char heurystyka = 'h';// 0 - brak heurystyki; h - metryka Hamminga; m - metryka Manhattan

            string fromFileName = null;

            string toFileName = null;

            string toFileStatName = null;


            if (args.Length == 0)
            {
                System.Console.WriteLine("Please enter a argument.");
                System.Console.WriteLine("pobrano dane z programu:");
                fromFileName = "dane";
                toFileName = " result";
                toFileStatName = "result_stat";
                System.Console.WriteLine("searchingAlgorithm: {0}", searchingAlgorithm);
                System.Console.WriteLine("childsOrder {0}", childsOrder.ToString());
                for (int i = 0; i < childsOrder.Length; i++)
                    System.Console.WriteLine("childsOrder {0}", childsOrder[i]);
                System.Console.WriteLine("heurystyka: {0}", heurystyka);
                System.Console.WriteLine("fromFileName: {0}", fromFileName);
                System.Console.WriteLine("toFileName: {0}", toFileName);
                System.Console.WriteLine("toFileStatName {0}", toFileStatName);
            }
            else
            {
                /*
                * Pobranie danych i nadpisanie ich do zmiennych z ewentualnych argumentow uruchomieniowych
                 */
                for (int i = 0; i < args.Length; i++)
                {
                    if (args[i] == "-b" || args[i] == "--bfs" || args[i] == "b" || args[i] == "bfs")
                    {
                        searchingAlgorithm = 'b';
                        if (args.Length > i)
                        {//sprawdzam, czy przekazano argument bedacy byc moze kolejnoscia przeszukiwania
                            childsOrder = args[i + 1].ToCharArray();

                            i++;//pobralem kolejny argument, wiec go pomijam
                        }

                    }
                    else if (args[i] == "-d" || args[i] == "--dfs" || args[i] == "d" || args[i] == "dfs")
                    {
                        searchingAlgorithm = 'd';
                        if (args.Length > i)
                        {//sprawdzam, czy przekazano argument bedacy byc moze kolejnoscia przeszukiwania
                            childsOrder = args[i + 1].ToCharArray();
                            i++;//pobralem kolejny argument, wiec go pomijam
                        }
                    }
                    else if (args[i] == "-astr" || args[i] == "astr")
                    {
                        searchingAlgorithm = 'a';
                        if (args.Length > i)
                        {//sprawdzam, czy przekazano argument bedacy byc moze heurystyka
                            if (args[i + 1] == "hamm")
                                heurystyka = 'h';
                            else if (args[i + 1] == "manh")
                                heurystyka = 'm';
                        }
                        i++;//pobralem kolejny argument, wiec go pomijam
                    }

                    else if (args.Length > i)
                    {

                        //sprawdzam, czy przekazano argument bedacy byc moze nazwa pliku

                        fromFileName = args[i];
                        i++;

                        toFileName = args[i];
                        i++;

                        toFileStatName = args[i];

                        i++;//pobralem kolejny argument, wiec go pomijam
                    }


                }
            }
            int[,] tab = ArrayFromFile("/../" + fromFileName);

            Puzzle puzzle = new Puzzle(4, tab);
            Solution solution = new Solution();

            if (searchingAlgorithm == 'b')
            {
                BFS roz = new BFS(new Node(puzzle, ""), childsOrder);
                solution = roz.findSolution();

            }
            else if (searchingAlgorithm == 'd')
            {
                DFS roz = new DFS(new Node(puzzle, ""), childsOrder);
                solution = roz.findSolution();
            }
            else if (searchingAlgorithm == 'a')
            {
                Astar roz = new Astar(new Node(puzzle, ""), heurystyka);
                solution = roz.findSolution();
            }
            else
            {
                Console.WriteLine("wrong parameter");
            }


            writeFile(solution.solution, toFileName);

            writeFile((solution.solutionLength + " " +
                solution.ProcessedStates.ToString() + " " +
                solution.VisitedStates.ToString() + " " +              
                solution.depth.ToString() + " " +
                solution.getWorkingTime().ToString()
                ).ToString(), toFileStatName);

        }

        public static int[,] ArrayFromFile(string path)
        {

            System.IO.TextReader reader = System.IO.File.OpenText(path);
            String input = reader.ReadLine();
            String[] numInLine = input.Split(' ');
            int[,] toReturn = new int[int.Parse(numInLine[0]), int.Parse(numInLine[1])];


            for (int i = 0; i < 4; i++)
            {

                input = reader.ReadLine();
                numInLine = input.Split(' ');
                for (int j = 0; j < numInLine.Length; j++)
                {
                    toReturn[i, j] = int.Parse(numInLine[j]);
                }

            }

            return toReturn;
        }


        public static void writeFile(string toWrite, string filename)
        {
            toWrite += "\n";
            System.IO.StreamWriter file = new System.IO.StreamWriter("/../" + filename);
            file.WriteLine(toWrite);

            file.Close();
        }

        public static void writeFileStats(string toWrite, string filename)
        {
            toWrite += "\n";
            System.IO.StreamWriter file = new System.IO.StreamWriter("/../" + filename);
            file.WriteLine(toWrite);

            file.Close();
        }


    }
}


