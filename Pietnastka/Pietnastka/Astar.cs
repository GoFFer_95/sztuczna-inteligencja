﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Priority_Queue;

namespace Pietnastka
{
    class Astar
    {
        private Node startNode;
        public Solution sol;
        private char heuristic;
        private SimplePriorityQueue<Node> priorityQueue;
        private char[] childsOrder;
        int depth;

        public Astar(Node startNode, char heuristic)
        {
            this.startNode = startNode;
            this.heuristic = heuristic;
            sol = new Solution();
            priorityQueue = new SimplePriorityQueue<Node>();
            priorityQueue.Enqueue(this.startNode, 0);
            childsOrder = new char[4] { 'R', 'U', 'D', 'L' };
            depth = 0;
        }

        public Solution Sol
        {
            get
            {
                return sol;
            }
        }

        public Solution findSolution()
        {
            List < Node > nodesVisited = new List<Node>();
            

            if (heuristic == 'm')
            {
                Manhattan(nodesVisited);
            }
            if (heuristic == 'h')
            {
                Hamming(nodesVisited);
            }

            sol.solution = startNode.previousSteps.Length + " " + startNode.previousSteps;
            sol.solutionLength = startNode.previousSteps.Length;

            if (startNode.previousSteps.Length > depth)
            {
                sol.depth = startNode.previousSteps.Length;
            }
            else
            {
                sol.depth = depth;
            }
            return sol;

        }

        public void Manhattan(List<Node> nodesVisited )
        {
            sol.ProcessedStates++;
            Node tempNode;
            // sol.startWorkingTime();
            // while (startNode.puzzle.CheckStatus(startNode.puzzle.Size, startNode.puzzle.TabState) == false)
            // {
                // foreach (char direction in childsOrder)
                // {
                    // if (this.startNode.puzzle.CanIGo(direction) == true)
                    // {
                        // tempNode = new Node(this.startNode.puzzle.newPuzzleAfterMove(direction),
                            // (startNode.previousSteps + direction).ToString());

                        // if (nodesVisited.Contains<Node>(tempNode) == false)
                        // {
                            // if (priorityQueue.Contains<Node>(tempNode) == false)
                            // {
                                //Tutaj algorytm nadawania priorytetu i dodanie
                                //do kolejki priorytetowej
                                
                                //priorityQueue.Enqueue(tempNode);
                                // Console.WriteLine("dodane");
                                // tempNode.puzzle.PrintTab();
                            // }
                        // }
                    // }
                // }
                // nodesVisited.Add(startNode);
                // sol.VisitedStates += 1;

                // this.startNode = priorityQueue.Dequeue();
            // }
            // sol.stopWorkingTime();
			
			sol.startWorkingTime();
            while (priorityQueue.Count != 0)
            {
                startNode = priorityQueue.Dequeue();
               
                foreach (char direction in childsOrder)
                {
                    int priorityValue = 0;
                    if (startNode.puzzle.CanIGo(direction))
                    {
                        tempNode = new Node(startNode.puzzle.newPuzzleAfterMove(direction), startNode.previousSteps + direction);

                        if (tempNode.puzzle.CheckStatus(tempNode.puzzle.Size, tempNode.puzzle.TabState))
                        {
                            //koniec programu
                            sol.stopWorkingTime();
                            startNode.previousSteps = startNode.previousSteps + direction;
                            sol.solution = startNode.previousSteps.Length + " " + startNode.previousSteps;
                            sol.solutionLength = startNode.previousSteps.Length;

                            if (startNode.previousSteps.Length > depth)
                            {
                                sol.depth = startNode.previousSteps.Length;
                            }
                            else
                            {
                                sol.depth = depth;
                            }
                            return;
                        }
                        else if (!priorityQueue.Contains(tempNode) && !nodesVisited.Contains(tempNode))
                        {

                            //Algorytm nadawania priorytetu i dodania do kolejki
                            int actPositionValue = 0;
                            for (int i = 0; i < startNode.puzzle.Size; i++)
                            {

                                for (int j = 0; j < startNode.puzzle.Size; j++)
                                {
                                    actPositionValue++;
                                    if (i == 3 && j == 3)
                                        actPositionValue = 0;
                                    if (startNode.puzzle.TabState[i, j] != actPositionValue)
                                    {
                                        int tempActPositionValue = actPositionValue;
                                        for (int m = 0; m < startNode.puzzle.Size; m++)
                                        {

                                            for (int n = 0; n < startNode.puzzle.Size; n++)
                                            {
                                                
                                                if (startNode.puzzle.TabState[m, n] == tempActPositionValue)
                                                {
                                                    int tempPriority = Math.Abs(i - m) + Math.Abs(j - n);
                                                    priorityValue += tempPriority;
                                                   
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            priorityQueue.Enqueue(tempNode, priorityValue + tempNode.previousSteps.Count());
                            if (tempNode.previousSteps.Length > depth)
                            {
                                depth = startNode.previousSteps.Length;
                            }
                            sol.ProcessedStates++;
							
							
                        }
                    }
                    
                }
                nodesVisited.Add(startNode);
                sol.VisitedStates++;
            }
            sol.stopWorkingTime();

            sol.solution = "-1";
            sol.solutionLength = 0;
            sol.depth = depth;

        }

        public void Hamming(List<Node> nodesVisited )
        {
            sol.ProcessedStates++;
            Node tempNode;
            sol.startWorkingTime();
            while (priorityQueue.Count != 0)
            {
                startNode = priorityQueue.Dequeue();
               
                foreach (char direction in childsOrder)
                {
                    int priorityValue = 0;

                    if (startNode.puzzle.CanIGo(direction))
                    {
                        tempNode = new Node(startNode.puzzle.newPuzzleAfterMove(direction), startNode.previousSteps + direction);

                        if (tempNode.puzzle.CheckStatus(tempNode.puzzle.Size, tempNode.puzzle.TabState))
                        {
                            //koniec programu
                           
                            sol.stopWorkingTime();
                            startNode.previousSteps = startNode.previousSteps + direction;
                            sol.solution = startNode.previousSteps.Length + " " + startNode.previousSteps;
                            sol.solutionLength = startNode.previousSteps.Length;

                            if (startNode.previousSteps.Length > depth)
                            {
                                sol.depth = startNode.previousSteps.Length;
                            }
                            else
                            {
                                sol.depth = depth;
                            }
                            return;
                        }
                        else if (!priorityQueue.Contains(tempNode) && !nodesVisited.Contains(tempNode))
                        {
                            //Algorytm nadawania priorytetu i dodania do kolejki
                            int actPositionValue = 0;
                            for (int i = 0; i < startNode.puzzle.Size; i++)
                            {
                                
                                for (int j = 0; j < startNode.puzzle.Size; j++)
                                {
                                    actPositionValue++;
                                    
                                    if (i == 3 && j == 3)
                                        actPositionValue = 0;
                                    if (startNode.puzzle.TabState[i, j] != actPositionValue)
                                        priorityValue++;
                                }
                            }
                            priorityQueue.Enqueue(tempNode, priorityValue + tempNode.previousSteps.Count());
                            if (tempNode.previousSteps.Length > depth)
                            {
                                depth = startNode.previousSteps.Length;
                            }
                            sol.ProcessedStates++;
							
							
                        }
                    }
                   
                }
                nodesVisited.Add(startNode);
                sol.VisitedStates++;
            }
            sol.stopWorkingTime();
			
			sol.solution = "-1";
            sol.solutionLength = 0;
            sol.depth = depth;



        }
    }
}
