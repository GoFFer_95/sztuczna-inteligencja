﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pietnastka
{
    class Puzzle 
    {
        int size;
        int[,] tabState;

        int positionZeroX { get; set; }
        int positionZeroY { get; set; }


        public Puzzle(int size, int [,] tab)
        {
            this.tabState = tab;
            this.size = size;

            //szukanie w tabState pozycji zera i zapisywnaie tego do tabeli lastPositions
            for(int i = 0; i< size; i++)
            {
                for(int j = 0; j<size; j++)
                {
                    if(tabState[i,j].Equals(0))
                    {
                        positionZeroX = i;
                        positionZeroY = j;
                    }
                }
            }
           
        }

        public void PrintTab()
        {
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < this.size; j++)
                {
                    System.Console.Write("{0} ", tabState[i, j]);
                }
                System.Console.WriteLine(" ");
               
            }
            System.Console.WriteLine(" ");
        }

        public Puzzle newPuzzleAfterMove(char direction)
        {
            int[,] newTab = new int[this.size, this.size]; //stworzenie nowej tablicy 

            for( int i = 0; i <this.size; i++) //kopiujemy tablicę
            {
                for(int j = 0; j< this.size; j++)
                {
                    newTab[i, j] = tabState[i, j];
                }
            }
            //sprawdzamy czy możemy przesunąc, jak możemy przesunąc to zmianiamy kolejnością dwa sąsiadujące pola

            
            switch (direction)
            {
                case 'U':
                    if (positionZeroX > 0)
                    {
                        newTab[positionZeroX, positionZeroY] = newTab[positionZeroX - 1, positionZeroY];
                        newTab[positionZeroX - 1, positionZeroY] = 0;
                    }
                    
                    break;

                case 'D':
                    if (positionZeroX < (size -1) )
                    {
                       
                        newTab[positionZeroX, positionZeroY] = newTab[positionZeroX + 1, positionZeroY];
                        newTab[positionZeroX + 1, positionZeroY] = 0;
                    }

                    break;


                case 'L':
                    if (positionZeroY > 0)
                    {
                        newTab[positionZeroX, positionZeroY] = newTab[positionZeroX , positionZeroY - 1];
                        newTab[positionZeroX , positionZeroY - 1] = 0;
                    }
                    
                    break;

                case 'R':
                    if (positionZeroY < (size -1) )
                    {
                        newTab[positionZeroX, positionZeroY] = newTab[positionZeroX, positionZeroY + 1];
                        newTab[positionZeroX, positionZeroY + 1] = 0;
                    }

                    
                    break;
            }

            Puzzle newPuzzle = new Puzzle(size, newTab);
            return newPuzzle;
            
        }

        public bool CanIGo(char direction)
        {

               // System.Console.Write("{0}, {1}, {2}", direction, positionZeroX, positionZeroY);
                switch (direction)
                {
                    case 'U':
                        if (positionZeroX > 0)
                        {
                        
                        return true;
                            
                        }
                        break;

                    case 'D':
                        if (positionZeroX < (size - 1))
                        {
                       
                        return true;

                    }
                        break;


                    case 'L':
                        if (positionZeroY > 0)
                        {
                      
                        return true;

                    }
                        break;

                    case 'R':
                        if (positionZeroY < (size - 1))
                        {
                      
                        return true;

                    }
                        break;
                }
            return false;

        }

        public bool CheckStatus(int size, int[,] tab)
        {
            for(int i = 0; i<size; i++)
            {
                for(int j = 0; j<size; j++)
                {
                    if(i == (size-1) && j ==(size -1) && tab[i, j] == 0)
                    {
                        return true;
                    }
                    else if(tab[i,j] != (1 + i * 4 + j ))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public int Size
        {
            get
            {
                return size;
            }
            set
            {
                size = value;
            }
        }

        public int[,] TabState
        {
            get
            {
                return tabState;
            }
        }

        public override bool Equals(Object other)
        {
            if(other == null)
            {
                return false;
            }

            Puzzle puzzleOther = other as Puzzle;

            return     tabState.Rank == puzzleOther.tabState.Rank &&
    Enumerable.Range(0, tabState.Rank).All(dimension => tabState.GetLength(dimension) == puzzleOther.tabState.GetLength(dimension)) &&
    tabState.Cast<int>().SequenceEqual(puzzleOther.tabState.Cast<int>());

        }

        public override int GetHashCode()
        {
            // TODO: write your implementation of GetHashCode() here
            //throw new NotImplementedException();

            int hash = 17;
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    hash = hash * 31 + tabState[i, j];
                }
            }
            return hash;
            //return tabState.GetHashCode();
        }

    }
}
